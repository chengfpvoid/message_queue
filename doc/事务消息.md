

### 事务消息





- MybatisTransactionMsgClient.java

```java
/**
* 通过事务保证本地事务与事务消息表写入原子性
*/ 
public abstract class TransactionMsgClient {
    private static final String MQProducerName = "TransactionMsgProducer";
    protected static final Logger log = LoggerFactory.getLogger(TransactionMsgClient.class);
    private DefaultMQProducer producer;
    private String mqAddr;
    private List<DBDataSource> dbDataSources;
    protected MsgProcessor msgProcessor;
    private MsgStorage msgStorage;
    private Config config;
    private AtomicReference<State> state;
    private List<String> topicLists;
    
    /**
     * 
     * @param mqAddr
     * @param config
     * @param topicLists 沙箱和线上区分，需要是主题名字不一样（沙箱和线上是同一个db）,如果为空，默认扫描当前msg表下面，所有主题
     * @param dbDataSources 数据源，用户名，密码，url Url需要和业务dataSource中配置的url一模一样
     */
    protected TransactionMsgClient(String mqAddr,List<DBDataSource> dbDataSources,List<String> topicLists,Config config){
        this.mqAddr = mqAddr;
        this.dbDataSources = dbDataSources;
        this.topicLists= topicLists;
        msgStorage = new MsgStorage(dbDataSources,topicLists);
        producer = new DefaultMQProducer(MQProducerName); // producer group
        producer.setNamesrvAddr(this.mqAddr); // mq 地址
        msgProcessor = new MsgProcessor(producer, msgStorage);
        this.config = config;
        state = new AtomicReference<State>(State.CREATE);

    }
    
    /**
     * 
     * @param content 事务消息内容
     * @param topic 主题
     * @param tag 
     * @return 事务消息插入库的id
     * @throws RuntimeException spring aop 事务默认配置，如果是抛出RuntimeException才会回滚，如果是Exception不会回滚
     */
    public abstract Long sendMsg(String content,String topic,String tag) throws Exception;
    
    /**
     * 初始化内部MQ Producer,mysql 连接池，线程池等
     * @throws MQClientException 
     */
    public void init() throws MQClientException{
        if(state.get().equals(State.RUNNING)){
            log.info("TransactionMsgClient have inited, return");
            return ;
        }
        log.info("start init mqAddr={} state {} this {}",this.mqAddr,state,this);
        producer.setSendMsgTimeout(config.getSendMsgTimeout());
        if(config == null)
            config = new Config();
        try{
            producer.start(); // 启动producer
            msgProcessor.init(this.config);
            msgStorage.init(this.config);
            log.info("end init success");
        }catch(MQClientException ex){
            log.error("producer start fail", ex);
            throw ex;
        } 
        state.compareAndSet(State.CREATE, State.RUNNING);
    }
    
    
    public void close(){
        log.info("start close TransactionMsgClient");
        if (state.compareAndSet(State.RUNNING, State.CLOSED)) {
            msgProcessor.close();
            msgStorage.close();
            producer.shutdown();
        } else
            log.info("state not right {} ", state);
    }


    /**
     * 
     * @param con 如果我们拿不到连接，需要暴露出来，让业务方set Connection
     * @param content
     * @param topic
     * @param tag
     * @return
     * @throws Exception
     */
    public Long sendMsg(Connection con,String content,String topic,String tag) throws Exception{
        Long id = null;
        if(!state.get().equals(State.RUNNING)){
            log.error("TransactionMsgClient not Running , please call init function");
            throw new Exception("TransactionMsgClient not Running , please call init function");
        }
        if(content == null || content.isEmpty() || topic == null || topic.isEmpty()){
            log.error("content or topic is null or empty");
            throw new Exception("content or topic is null or empty, notice ");
        }
        try{
            log.debug("insert to msgTable topic {} tag {} Connection {} Autocommit {} ",topic,tag,con,con.getAutoCommit());
            if(con.getAutoCommit()){
                log.error("***** attention not in transaction ***** topic {} tag {} Connection {} Autocommit {} ",topic,tag,con,con.getAutoCommit());
                throw new Exception("connection not in transaction con "+con);
            }
            
            //事务消息落库
            Map.Entry<Long, String> idUrlPair = MsgStorage.insertMsg(con, content, topic, tag);
            id = idUrlPair.getKey();
            Msg msg  = new Msg(id,idUrlPair.getValue());
            
            //异步发送消息
            msgProcessor.putMsg(msg);
        }catch(Exception ex){
            // TODO Auto-generated catch block
            log.error("sendMsg fail topic {} tag {} ",topic,tag, ex);
            throw ex;
        }
        return id;
    }
    
    public String getMqAddr() {
        return mqAddr;
    }

    public void setMqAddr(String mqAddr) {
        this.mqAddr = mqAddr;
    }

    public List<DBDataSource> getDbDataSources() {
        return dbDataSources;
    }

    public void setDbDataSources(List<DBDataSource> dbDataSources) {
        this.dbDataSources = dbDataSources;
    }

    public Config getConfig() {
        return config;
    }
    /**
     * 
     * @param config 连接池，线程，内部时间周期等参数
     */
    public void setConfig(Config config) {
        this.config = config;
    }

}

```









- MsgProcessor.java

```java


/**
 * 主要思想,一个按照创建时间排序的msg Queue，一个按照下次超时时间排序的时间轮队列.
 * 由定时线程扫描时间轮，重新放入msg Queue中，超过6次，直接丢弃，由离线的定时扫描做重试
 * 只有从DB里面查到了，才说明事务提交了
 * @author zzy
 *
 */
public class MsgProcessor {

    private static final int getTimeoutMs = 100;
    private static final int[] timeOutData = new int[]{0,5,10,25,50,100}; //5ms,10ms,25ms,50ms,100ms
    private static final int maxDealTime = 6;
    private static final int limitNum = 50;
    private static final int maxDealNumOneTime = 2000;
    private static final int timeWheelPeriod = 5;//5ms
    private static boolean envNeedLock = false;
    //private static boolean isTestEnv = false;
    private static final String canExeKey = "CanExe";
      private static final String deletePrefix = "delete_";
    private static final String selectPrefix = "select_";
    private static final int holdLockTime = 60 ;//60s
    private static final int sanboxTimes = 3;
    private static final int checkLockInitDelay = 10;
    private static final Logger log = LoggerFactory.getLogger(MsgProcessor.class);
    
    private PriorityBlockingQueue<Msg> msgQueue;
    private ExecutorService exeService;
   
    //private ThreadPoolExecutor msgExecutor;
    private PriorityBlockingQueue<Msg> timeWheelQueue;
    private ScheduledExecutorService scheService;
    private AtomicReference<State> state;
    private DefaultMQProducer producer;
    private MsgStorage msgStorage;
    private Config config;
    private ZZLockClient zzlockClient;
    private volatile boolean holdLock = true;
    private String lockKey = "defaultTransKey";
    private ServerType serverType = null;
    public MsgProcessor(DefaultMQProducer producer,MsgStorage msgStorage){
        //this.config = config;
        this.producer = producer;
        this.msgStorage = msgStorage;
        msgQueue = new PriorityBlockingQueue<Msg>(5000, new Comparator<Msg>(){
            public int compare(Msg o1, Msg o2) {
                // TODO Auto-generated method stub
                long diff = o1.getCreateTime() - o2.getCreateTime();
                if(diff>0)
                    return 1;
                else if(diff<0)
                    return -1;
                return 0;
            }
        });
        timeWheelQueue = new PriorityBlockingQueue<Msg>(1000, new Comparator<Msg>(){
            public int compare(Msg o1, Msg o2) {
                // TODO Auto-generated method stub
                long diff = o1.getNextExpireTime() - o2.getNextExpireTime();
                if(diff>0)
                    return 1;
                else if(diff<0)
                    return -1;
                return 0;
            }
        });
        state = new AtomicReference<State>(State.CREATE);
        
    }
    /**
     * init 阶段，config才是ok
     */
    public void init(Config config){
        if(state.get().equals(State.RUNNING)){
            log.info("Msg Processor have inited return");
            return ;
        }
        log.info("MsgProcessor init start");
        state.compareAndSet(State.CREATE, State.RUNNING);
        this.serverType = Util.getServerType();
        if(config.getEtcdHosts() != null && config.getEtcdHosts().length >=1 ){
            envNeedLock = true;
            defaultEtcd = config.getEtcdHosts();
        }

        log.info("serverType {} envNeedLock {} etcdhosts {}",serverType,envNeedLock,defaultEtcd);
        this.config = config;
        if(ServerType.Sandbox.equals(serverType)){
            this.config.setDeleteTimePeriod(this.config.getDeleteTimePeriod() * sanboxTimes);
            this.config.setSchedScanTimePeriod(this.config.getSchedScanTimePeriod() * sanboxTimes);
        }
        exeService = Executors.newFixedThreadPool(config.getThreadNum(), new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                // TODO Auto-generated method stub
                Thread thread = new Thread(r,"MsgProcessorThread");
                return thread;
            }
        });
        scheService = Executors.newScheduledThreadPool(config.getSchedThreadNum(), new ThreadFactory() {
            
            @Override
            public Thread newThread(Runnable r) {
                // TODO Auto-generated method stub
                Thread thread = new Thread(r,"MsgScheduledThread");
                return thread;
            }
        });
        for(int i=0;i<config.getThreadNum();i++){
            MsgProcessorRunnable runnable  = new MsgProcessorRunnable();
            exeService.submit(runnable);
        }
        scheService.scheduleAtFixedRate(new MsgTimeWheelRunnable(), timeWheelPeriod, timeWheelPeriod, TimeUnit.MILLISECONDS);
        scheService.scheduleAtFixedRate(new DeleteMsgRunnable(), config.deleteTimePeriod, config.deleteTimePeriod, TimeUnit.SECONDS);
        scheService.scheduleAtFixedRate(new SchedScanMsgRunnable(), config.schedScanTimePeriod, config.schedScanTimePeriod, TimeUnit.SECONDS);
        scheService.scheduleAtFixedRate(new Runnable() {
            
            @Override
            public void run() {
                // TODO Auto-generated method stub
                log.info("stats info msgQueue size {} timeWheelQueue size {}",msgQueue.size(),timeWheelQueue.size());
            }
        }, 20, config.getStatsTimePeriod(), TimeUnit.SECONDS);
        
        log.info("MsgProcessor init end");
    }
    
    protected void putMsg(Msg msg){
        msgQueue.put(msg);
    }

    private static Message buildMsg(final MsgInfo msgInfo) throws UnsupportedEncodingException{
        String topic = msgInfo.getTopic();
        String tag = msgInfo.getTag();
        String content = msgInfo.getContent();
        String id = msgInfo.getId()+"";
        Message msg = new Message(topic,tag,id,content.getBytes("UTF-8")); /* Message body */
        String header = String.format("{\"topic\":\"%s\",\"tag\":\"%s\",\"id\":\"%s\",\"createTime\":\"%s\"}",topic,tag,id,System.currentTimeMillis());
        msg.putUserProperty("MQHeader", header);
        return msg;
    }
    
    
     
   
    class MsgProcessorRunnable implements Runnable {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            while (true) {
                if(!state.get().equals(State.RUNNING))
                    break;
                //log.trace("msg Processor start run");
                Msg msg = null;
                try {
                    try{
                        msg = msgQueue.poll(getTimeoutMs, TimeUnit.MILLISECONDS);
                    }catch(InterruptedException ex){
                        
                    }
                    if (msg == null)
                        continue;
                    log.trace("poll msg {}", msg);
                    int dealedTime = msg.getHaveDealedTimes() + 1;
                    msg.setHaveDealedTimes(dealedTime);
                    MsgInfo msgInfo = msgStorage.getMsgById(msg);
                    // 这里我们不知道是否事务已经提交，所以需要从DB里面拿
                    log.trace("getMsgInfo from DB {}", msgInfo);
                    if(msgInfo == null ){
                        if(dealedTime < maxDealTime){
                            long nextExpireTime = System.currentTimeMillis() + timeOutData[dealedTime];
                            msg.setNextExpireTime(nextExpireTime);
                            // 加入时间轮检查队列
                            timeWheelQueue.put(msg);
                            log.trace("put msg in timeWhellQueue {} ",msg);
                        }
                    }else{
                        Message mqMsg = buildMsg(msgInfo);
                        SendResult result = producer.send(mqMsg);
                        log.info("msgId {} topic {} tag {} sendMsg result {}", msgInfo.getId(),mqMsg.getTopic(),mqMsg.getTags(),result);
                        if (null == result || result.getSendStatus() != SendStatus.SEND_OK) {
                            if (dealedTime < maxDealTime) {
                                long nextExpireTime = System.currentTimeMillis() + timeOutData[dealedTime];
                                msg.setNextExpireTime(nextExpireTime);
                                timeWheelQueue.put(msg);
                            }
                        } else if (result.getSendStatus() == SendStatus.SEND_OK) {
                            // 修改数据库的状态
                            int res = msgStorage.updateMsgStatus(msg);
                            log.trace("msgId {} updateMsgStatus success res {}", msgInfo.getId(), res);
                        }
                    }
                    
                }catch (Exception e) {
                    // TODO Auto-generated catch block
                    log.error("MsgProcessor deal msg fail",e);
                }
            }
        }

    }
    
    class MsgTimeWheelRunnable implements Runnable {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            try{
            if(state.get().equals(State.RUNNING)){
                //log.trace("msgTimeWheelRun start run");
                long cruTime = System.currentTimeMillis();
                Msg msg = timeWheelQueue.peek();
                // 拿出来的时候有可能还没有超时
                while(msg != null && msg.getNextExpireTime() <= cruTime){
                    msg = timeWheelQueue.poll();
                    log.trace("timeWheel poll msg ,return to msgQueue {}",msg);
                    msgQueue.put(msg);// 重新放进去
                    msg = timeWheelQueue.peek();
                }
                
            }
            }catch(Exception ex){
                log.error("pool timequeue error",ex);
            }
        }   
    }
    
    //后台任务，从数据库中删除已发送的消息
    class DeleteMsgRunnable implements Runnable{
        @Override
        public void run() {
            // TODO Auto-generated method stub
            if(state.get().equals(State.RUNNING)){
                log.trace("DeleteMsg start run");
                try{
                    HashMap<String, DataSource> data = msgStorage.getDataSourcesMap();
                    Collection<DataSource> dataSrcList = data.values();
                    Iterator<DataSource> it = dataSrcList.iterator();
                    while(it.hasNext()){
                        DataSource dataSrc = it.next();
                        boolean canExe = holdLock;
                        if(canExe){
                            log.info("DeleteMsgRunnable run ");
                            int count = 0 ;
                            int num = config.deleteMsgOneTimeNum;
                            while(num == config.deleteMsgOneTimeNum && count < maxDealNumOneTime){
                                try {
                                    num = msgStorage.deleteSendedMsg(dataSrc, config.deleteMsgOneTimeNum);
                                    count += num;
                                } catch (SQLException e) {
                                    // TODO Auto-generated catch block
                                    log.error("deleteSendedMsg fail ",e);
                                }
                            }
                        }
                    }
                }catch(Exception ex){
                    log.error("delete Run error ",ex);
                }
            }
        }
        
    }
    
    
    
    
    
    class SchedScanMsgRunnable implements Runnable{

        @Override
        public void run() {
            if (state.get().equals(State.RUNNING)) {
                log.trace("SchedScanMsg start run");
                HashMap<String, DataSource> data = msgStorage.getDataSourcesMap();
                Collection<DataSource> dataSrcList = data.values();
                Iterator<DataSource> it = dataSrcList.iterator();
                while (it.hasNext()) {
                    DataSource dataSrc = it.next();
                    boolean canExe = holdLock;
                    if(canExe){
                        log.info("SchedScanMsgRunnable run");
                        int num = limitNum;
                        int count = 0;
                        while (num == limitNum && count < maxDealNumOneTime) {
                            try {
                                List<MsgInfo> list = msgStorage.getWaitingMsg(dataSrc, limitNum);
                                num = list.size();
                                if (num > 0)
                                    log.trace("scan db get msg size {} ", num);
                                count += num;
                                for (MsgInfo msgInfo : list) {
                                    try {
                                        Message mqMsg = buildMsg(msgInfo);
                                        
                                        //向MQ发送消息
                                        SendResult result = producer.send(mqMsg);
                                        log.info(....)
                                        if (result != null && result.getSendStatus() == SendStatus.SEND_OK) {
                                            // 修改数据库的状态
                                            int res = msgStorage.updateMsgStatus(dataSrc, msgInfo.getId());
                                            
                                        }
                                    } catch (Exception e) {
                                        log.error("SchedScanMsg deal fail", e);
                                    }
                                }
                            } catch (SQLException e) {
                                // TODO Auto-generated catch block
                                log.error("getWaitMsg fail", e);
                            }
                        }
                    }
                }
            }
        }

    }

}
```







- MybatisTransactionMsgClient.java

```java
/**
* 提供给客户端使用
*/
public class MybatisTransactionMsgClient extends TransactionMsgClient{

    private SqlSessionTemplate sessionTemplate;
    
    public MybatisTransactionMsgClient(SqlSessionTemplate sessionTemplate,String mqAddr,List<DBDataSource> dbDataSources,List<String> topicLists) {
        super(mqAddr,dbDataSources,topicLists,new Config());
        this.sessionTemplate = sessionTemplate;
    }
    public MybatisTransactionMsgClient(SqlSessionFactory sqlSessionFactory,String mqAddr,List<DBDataSource> dbDataSources,List<String> topicLists,Config config){
        super(mqAddr,dbDataSources,topicLists,config);
        //this.sqlSessionFactory = sqlSessionFactory;
        try {
            this.sessionTemplate = new SqlSessionTemplate(sqlSessionFactory);
        } catch (Exception e) {
            //  Auto-generated catch block
            log.error("get sqlSessionFactory fail",e);
        }
    }
    public MybatisTransactionMsgClient(SqlSessionFactory sqlSessionFactory,String mqAddr,List<DBDataSource> dbDataSources,List<String> topicLists){
        this(sqlSessionFactory, mqAddr, dbDataSources, topicLists,new Config());
    }

    //客户端发送消息方法
    public Long sendMsg(String content, String topic, String tag) throws Exception {
        //  Auto-generated method stub
        Long id = null;
        try {
            Connection con = sessionTemplate.getConnection();
            id = super.sendMsg(con, content, topic, tag);
            return id;
        } catch (Exception ex) {
            //  Auto-generated catch block
            log.error("sendMsg fail topic {} tag {} ", topic,tag,ex);
            throw new RuntimeException(ex);
        }
    }

    public SqlSessionTemplate getSessionTemplate() {
        return sessionTemplate;
    }

    public void setSessionTemplate(SqlSessionTemplate sessionTemplate) {
        this.sessionTemplate = sessionTemplate;
    }
    
    public void setHistoryMsgStoreTime(int historyMsgStoreTime) {
        this.getConfig().setHistoryMsgStoreTime(historyMsgStoreTime);
    }
    
    public int getHistoryMsgStoreTime() {
        return this.getConfig().getHistoryMsgStoreTime();
    }

}
```

